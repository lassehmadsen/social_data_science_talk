---
title: "Notes for talk"
author: "Lasse Hjorth Madsen"
date: "14/08/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Talk for students at the Social Data Science programme

### Topic: How to use tools from Social Data Science at Novo Nordisk?

~ 30 mins. Structure and ideas:

1. **Who am I?**  
Masters in Political Science, cand.scient.pol., from 1997

2. **The problem with social science**  
[*slide: Social Science is broken*]  
[*slide: Three reasons*]  
    a. Relies too heavily on questionaire data
        - Example 1: The context effect
        - Example 2: The Eurobarometer
        - Example 3: The Election Survey  
\ 
    b. Not enough emphasis on sound statistics and mathematics
        - Example: "Model searching" in SPSS  
\ 
    c. Not committed to proven methodology from the natural sciences
        - Note the tremendous track record from the natural sciences  
\   
3. **The good news: Social Data Science can fix all of that**  
[*slide: Data Science can fix that*] 

4. **The Deviations Explorer project**  
    * The concept of deviations in the medical industry  
    * Why it is important  
\
5. **Natual Language Processing, NLP**  

6. **How to turn text into numerical data?**  
[*slide: document-term-matrix*]

7. **How to compare texts**  
[*slide: cosine similarity*]

7. **How to count words?**  
[*Slide: tf-idf*] 
If you doubt that this is useful, consider the Google revenue of 66,001,000,000 US dollar (2014)   

8. **The power law of word frequencies**  
[*Slide: Harry Potter, Bible, Quran, Jane Austen*]

9. **Business Intelligence or Data Science?**  
[*Henrik Fylding-plot or similar*]
